﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuCore : MonoBehaviour
{
    public GameObject progress;
    private AsyncOperation async;
    private bool startLoading = false;
    public Image loadingProgress;

    void Start()
    {
        Time.timeScale = 1f;
    }

    public void Exit()
    {
        Debug.Log("EXIT PRESSED!");
        Application.Quit();
    }

    public void Start25()
    {
        progress.SetActive(true);
        startLoading = true;
        PlayerPrefs.SetInt("speedMode", 0);
        PlayerPrefs.Save();
        StartCoroutine(LoadGamescene());
    }

    public void Start5()
    {
        progress.SetActive(true);
        startLoading = true;
        PlayerPrefs.SetInt("speedMode", 1);
        PlayerPrefs.Save();
        StartCoroutine(LoadGamescene());
    }

    public void Start1()
    {
        progress.SetActive(true);
        startLoading = true;
        PlayerPrefs.SetInt("speedMode", 2);
        PlayerPrefs.Save();
        StartCoroutine(LoadGamescene());
    }

    IEnumerator LoadGamescene()
    {
        async = SceneManager.LoadSceneAsync("Game");
        yield return null;
    }

    void FixedUpdate()
    {
        if (startLoading)
        {
            float curFilling = async.progress/1f;
            loadingProgress.fillAmount = curFilling;
        }
    }
}
