﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonData : MonoBehaviour
{

    [SerializeField]
    private AudioClip sound;
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private Manager manager;
    [SerializeField]
    private float coloringStart = 0f;
    [SerializeField]
    private float pressStart = 0f;
    [SerializeField]
    private float pressDelay = 0f;
    private SpriteRenderer renderer;
    [SerializeField]
    private Sprite idle;
    [SerializeField]
    private SpriteRenderer readyToClick;
    [SerializeField]
    private Sprite clicked;
    private float clickedEnd = 0f;
    private float clickedEndTime = 2.2f;
    private bool needToClickedChange = false;
    [SerializeField]
    private float playingDelay = 0.02f;
    private bool playing = false;
    private float lastPlayed = 0f;
    private bool isCanPlay = true;
    [SerializeField]
    private float timerAdd = 0f;
    [SerializeField]
    int counteric = 0;
    [SerializeField]
    private List<float> startTime = new List<float>();
    [SerializeField]
    private List<float> endTime = new List<float>();

    [SerializeField]
    private bool wStart = true;
    [SerializeField]
    private bool wEnd = false;
    [SerializeField]
    private bool changedPic = false;
    [SerializeField]
    private bool changedPrepare = false;
    [SerializeField]
    private bool changedLongPrepare = false;
    private bool playAble = true;
    int counter = 0;
    private bool colorising = false;
    private Vector3 colorAdding = Vector3.zero;
    [SerializeField]
    private CounterManager counterManager;
    private bool countered = false;
    private bool turned = true;
    bool checkedTime = false;
    private float timeToColor = 0.3f;
    private float longTimeToColor = 1.5f;
    private bool lastNeedGreen = false;

    private GameObject managerGO;
    [SerializeField]
    private bool isCanBeQueued = true;
    [SerializeField]
    private bool haveToClick = false;
    private float haveToClickTimeStart = 0f;
    private bool haveToClickYellow = false;
    [SerializeField]
    private bool clickedBefore = false;
    private float pressStartTime = 0f;
    private float pressEndTime = 0f;
    private bool canselGreen = false;
    private float lastEndTime = 0f;
    private bool beforeProclicked = false;
    [SerializeField]
    private bool haveToClickLastGreen = false;
    private bool ignoreCheckingLastGreen = false;
    private bool counted = false;
    private bool checked2 = true;

    private int curSoundIndex = 0;
    private int lastIndexAdded = -1;
    public float lastWrongPressed = 0f;

    private float speeder = 1f;

    void Start()
    {
        if (this.gameObject.name != "ww")
        {
            counterManager = this.gameObject.transform.GetChild(0).gameObject.GetComponent<CounterManager>() as CounterManager;
        }
        managerGO = GameObject.FindGameObjectWithTag("Manager");
        manager = managerGO.GetComponent<Manager>();
        renderer = this.gameObject.GetComponent<SpriteRenderer>() as SpriteRenderer;
        source = this.gameObject.GetComponent<AudioSource>() as AudioSource;
        changedPrepare = false;
        changedLongPrepare = false;
        changedPic = false;
        wStart = true;
        timeToColor = managerGO.GetComponent<Manager>().GetTimeToColor();
        longTimeToColor = managerGO.GetComponent<Manager>().GetLongTimeToColor();
        CheckData();

        int _timeMode = PlayerPrefs.GetInt("speedMode", 2);
        switch (_timeMode)
        {
            case 0:
                speeder = 1f/0.5f;
                // Time.timeScale = 0.5f;
                //source.PlayOneShot(music[0]);
                //source.Play();
                break;
            case 1:
                speeder = 1f / 0.75f;
                // Time.timeScale = 0.75f;
                //source.PlayOneShot(music[1]);
                //source.Play();
                break;
            case 2:
                // Time.timeScale = 1f;
                speeder = 1f;
                //source.PlayOneShot(music[2]);
                //source.Play();
                break;
        }
    }


    void CheckData()
    {
       /* int j;
        if (startTime.Count != endTime.Count)
        {
            //Debug.Log(this.gameObject.name + " START COUNT ISNT END TIME");
        }
        else
        {
            for (j = 0; j < startTime.Count; j++)
            {
                if (endTime[j] - startTime[j] >= 3f)
                {
                    //Debug.Log(this.gameObject.name + " IS ENDTIME-START TIME = " + (endTime[j] - startTime[j]));
                }
            }
        }
        checked2 = true;*/
    }

    void Update()
    {
        bool processedStart = false;
        if(!checked2 && Time.timeSinceLevelLoad > 2f)
        {
            CheckData();
        }
        if (startTime.Count > 0)
        {
            if (wStart)
            {
                processedStart = true;
                if (!changedLongPrepare)
                {
                    if (startTime[0] * speeder - longTimeToColor * speeder <= Time.timeSinceLevelLoad)
                    {
                        beforeProclicked = false;
                        if (!checkedTime)
                        {
                            CheckForTiming();
                        }
                        changedLongPrepare = true;
                        renderer.color = Color.grey;
                        float longer = 0f;
                        if (!colorising)
                        {
                            if (startTime.Count > 1)
                            {
                                // GREEN
                                if (startTime[1] * speeder - startTime[0] * speeder < 0.5f * speeder)
                                {
                                    lastNeedGreen = true;
                                    //Debug.Log("LAST NEED GREEN " + this.gameObject.name + " " + startTime[1] + " - " + startTime[0]);
                                    colorising = true;
                                    CountCounteric();
                                    colorAdding = new Vector3(Color.green.r - renderer.color.r, Color.green.g - renderer.color.g, Color.green.b - renderer.color.b);
                                    colorAdding = (colorAdding / (startTime[0] * speeder - timeToColor * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;
                                }
                                else
                                {
                                    canselGreen = true;
                                    //RED
                                    if (endTime[0] * speeder - startTime[0] * speeder <= 0.8f * speeder)
                                    {
                                        ClearCounteric();
                                        if (isCanBeQueued)
                                        {
                                            GameObject.FindWithTag("Manager").GetComponent<Manager>().AddButtonQueue(this.gameObject);
                                        }
                                        colorising = true;
                                        colorAdding = new Vector3(Color.red.r - renderer.color.r, Color.red.g - renderer.color.g, Color.red.b - renderer.color.b);
                                        colorAdding = (colorAdding / (startTime[0] * speeder - timeToColor * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;
                                        if (isCanBeQueued)
                                        {
                                            GameObject.FindWithTag("Manager").GetComponent<Manager>().AddButtonQueue(this.gameObject);
                                        }
                                    }
                                    //YELLOW
                                    else
                                    {
                                        ClearCounteric();
                                        colorising = true;
                                        colorAdding = new Vector3(Color.yellow.r - renderer.color.r, Color.yellow.g - renderer.color.g, Color.yellow.b - renderer.color.b);
                                        colorAdding = (colorAdding / (startTime[0] * speeder - timeToColor * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;
                                    }

                                }
                            }
                            else
                            {
                                //RED
                                if (endTime[0] * speeder - startTime[0] * speeder <= 0.8f * speeder)
                                {
                                    if (isCanBeQueued)
                                    {
                                        GameObject.FindWithTag("Manager").GetComponent<Manager>().AddButtonQueue(this.gameObject);
                                    }
                                    colorising = true;
                                    colorAdding = new Vector3(Color.red.r - renderer.color.r, Color.red.g - renderer.color.g, Color.red.b - renderer.color.b);
                                    colorAdding = (colorAdding / (startTime[0] * speeder - timeToColor * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;
                                }
                                //YELLOW
                                else
                                {
                                    colorising = true;
                                    colorAdding = new Vector3(Color.yellow.r - renderer.color.r, Color.yellow.g - renderer.color.g, Color.yellow.b - renderer.color.b);
                                    colorAdding = (colorAdding / (startTime[0] * speeder - timeToColor * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;
                                }
                            }
                        }
                    }
                    else
                    {
                        counterManager.ChangeTo(-1);
                        renderer.color = Color.white;
                    }
                }
                else if (!changedPrepare)
                {
                    haveToClick = true;

                    if (startTime[0] * speeder - timeToColor * speeder <= Time.timeSinceLevelLoad)
                    {
                        float longer = 0f;
                        colorising = false;
                        changedPrepare = true;

                        if (startTime.Count > 1)
                        {
                            // GREEN
                            if (startTime[1] * speeder - startTime[0] * speeder < 0.5f * speeder)
                            {
                                //colorising = true;
                                renderer.color = Color.green;
                            }
                            else
                            {
                                if (endTime[0] * speeder - startTime[0] * speeder <= 0.8f * speeder)
                                {
                                    // GREEN
                                    if (lastNeedGreen)
                                    {
                                        // lastNeedGreen = false;
                                        counterManager.ChangeTo(0);
                                        renderer.color = Color.green;
                                        //Debug.Log("GREENDED" + this.gameObject.name);
                                    }
                                    //RED
                                    else
                                    {
                                        renderer.color = Color.red;
                                    }
                                }
                                else
                                {
                                    // GREEN
                                    if (lastNeedGreen)
                                    {
                                        counterManager.ChangeTo(0);
                                        //  lastNeedGreen = false;
                                        renderer.color = Color.green;
                                        //Debug.Log("GREENDED" + this.gameObject.name);
                                    }
                                    //YELLOW
                                    else
                                    {
                                        renderer.color = Color.yellow;
                                    }
                                }

                            }
                        }
                        else
                        {
                            //RED
                            if (endTime[0] * speeder - startTime[0] * speeder <= 0.8f * speeder)
                            {
                                // GREEN
                                if (lastNeedGreen)
                                {
                                    //  lastNeedGreen = false;
                                    counterManager.ChangeTo(0);
                                    renderer.color = Color.green;
                                }
                                //RED
                                else
                                {
                                    renderer.color = Color.red;
                                }
                            }
                            //YELLOW
                            else
                            {
                                // GREEN
                                if (lastNeedGreen)
                                {
                                    counterManager.ChangeTo(0);
                                    // lastNeedGreen = false;
                                    renderer.color = Color.green;
                                }
                                //YELLOW
                                else
                                {
                                    renderer.color = Color.yellow;
                                }
                            }
                        }

                    }
                }
                else if (!changedPic)
                {
                    if (Time.timeSinceLevelLoad >= startTime[0] * speeder)
                    {
                        counted = false;
                        changedPic = true;
                        colorising = true;
                        //changedLongPrepare = false;
                        //changedPrepare = false;
                        //haveToClick = true;
                        wStart = false;
                        wEnd = true;

                        if (renderer.color == Color.yellow)
                        {
                            haveToClickYellow = true;
                        }
                        else
                        {
                            haveToClickYellow = false;
                        }
                        colorAdding = new Vector3(Color.white.r - renderer.color.r, Color.white.g - renderer.color.g, Color.white.b - renderer.color.b);
                        colorAdding = (colorAdding / (endTime[0] * speeder - (Time.timeSinceLevelLoad))) * Time.deltaTime;


                        MakeSoundChecking();
                        haveToClickTimeStart = Time.timeSinceLevelLoad;
                        startTime.RemoveAt(0);
                        CheckForStartTime();
                        if (endTime[0] * speeder - Time.timeSinceLevelLoad >= 3f)
                        {
                            if (endTime.Count - startTime.Count > 1)
                            {
                                manager.DebugTextChange(this.gameObject.name + " END TIME COUNT MORE THAT 1");
                            }
                            else if (endTime.Count - startTime.Count < 1)
                            {
                                manager.DebugTextChange(this.gameObject.name + " END TIME COUNT LESS THAT 1");
                            }
                            else
                            {

                                manager.DebugTextChange(this.gameObject.name + " IS bAD END TIME!");
                            }
                        }
                    }
                }
            }
        }
        if (endTime.Count > 0 && !processedStart)
        {

            if (wEnd)
            {

                if (Time.timeSinceLevelLoad >= endTime[0] * speeder)
                {
                    //Debug.Log("add error "+this.name);
                    //CheckForEndTime();
                    CheckForSoundEnd();
                    endTime.RemoveAt(0);
                    wEnd = false;
                    wStart = true;
                    changedPic = false;
                    changedLongPrepare = false;
                    changedPrepare = false;
                    colorising = false;

                    renderer.color = Color.white;
                    haveToClick = false;
                    clickedBefore = false;
                    lastEndTime = Time.timeSinceLevelLoad;
                    CountCounteric();
                }
            }
        }
        if (colorising)
        {
            Color newer = new Color(renderer.color.r + colorAdding.x, renderer.color.g + colorAdding.y, renderer.color.b + colorAdding.z);
            renderer.color = newer;
        }
    }


    void CheckForEndTime()
    {

        List<float> toSave = new List<float>();
        foreach (float eTime in endTime)
        {
            if (Time.timeSinceLevelLoad >= eTime)
            { 
                //Debug.Log("DELETED END TIME");
            }
            else
            {
                toSave.Add(eTime);
            }
        }
        endTime = toSave;
    }

    void CheckForStartTime()
    {
        return;
        List<float> toSave = new List<float>();
        foreach (float sTime in startTime)
        {
            if (Time.time > sTime + timerAdd)
            {
                //startTime.Remove(sTime);
               // Debug.L("DELETED START TIME");
            }
            else
            {
                toSave.Add(sTime);
            }
        }
        startTime = toSave;
    }

    void MakeButtonStartState()
    {
        wEnd = false;
        wStart = true;
        changedPic = false;
        changedLongPrepare = false;
        changedPrepare = false;
        colorising = false;

        renderer.color = Color.white;
        haveToClick = false;
        clickedBefore = false;
        lastEndTime = Time.timeSinceLevelLoad;
        CountCounteric();
    }

    public void MakeSound()
    { 
        if (sound != null)
        {
            if (haveToClick)
            {
                if (counteric > 0)
                {
                    if (startTime.Count > 0)
                    {
                        haveToClick = false;
                    }
                }
                else
                {
                    manager.DeleteButton(this.gameObject);
                    if (counteric == 0)
                    {
                        counterManager.ChangeTo(-1);
                    }
                    haveToClick = false;
                    pressStartTime = Time.timeSinceLevelLoad;
                    CountCounteric();
                }
            }
            else
            {
                if (haveToClickLastGreen)
                {
                }
                else
                {
                    if (startTime.Count < 1)
                    {
                        return;
                    }
                    if (startTime[0] * speeder - Time.timeSinceLevelLoad <= 0.75f * speeder && counteric == -1) // 0.45 - already colored, ~0.8 - starting coloring
                    {
                       // Debug.Log("CLICKED BEFORE!"+this.name);
                        clickedBefore = true;
                        haveToClick = false;
                        pressStartTime = Time.timeSinceLevelLoad;
                        manager.DeleteButton(this.gameObject);
                    }
                    else
                    {
                        if (lastNeedGreen)
                        {
                            renderer.color = Color.green;
                            counterManager.ChangeTo(0);
                            lastNeedGreen = false;
                            haveToClickLastGreen = true;
                        }
                        else
                        {
                            if (Time.timeSinceLevelLoad - lastWrongPressed > 1f * speeder)
                            {
                                manager.AddError();
                                lastWrongPressed = Time.timeSinceLevelLoad;
                            }
                            else
                            {
                                //kek :)
                            }
                        }
                    }
                }
                
            }
        }
        ignoreCheckingLastGreen = false;
    }

    public void StopSound()
    {

    }

    void MakeSoundChecking()
    {

    }

    public int GetCurrentQueuer()
    {
        return counterManager.GetCurrentNumber();
    }

    public void UpdateQueuer(int num)
    {
        counterManager.ChangeTo(num);
    }

    public void UpdateTimer(float value)
    {
        timerAdd = value;
    }

    public void AddStartTime(float value)
    {
        startTime.Add(value);
    }

    public void AddEndTime(float value)
    {
        endTime.Add(value);
    }

    public void MakeSoundAble(bool value)
    {
        playAble = value;
    }

    void CheckForSoundEnd()
    {
        if (haveToClick)
        {
           // Debug.Log("It have to be clicked " + this.name + " CB:"+clickedBefore);
            if (!clickedBefore && lastIndexAdded!=curSoundIndex)
            {
                lastIndexAdded = curSoundIndex;

                manager.AddError();
                haveToClick = false;
                haveToClickYellow = false;
                clickedBefore = false;
            }
            else
            {
                haveToClick = false;
                haveToClickYellow = false;
                clickedBefore = false;
            }
        }

        renderer.color = Color.white;
        needToClickedChange = false;
        haveToClick = false;
        haveToClickYellow = false;
        clickedBefore = false;
        wStart = true;
        wEnd = false;
        changedPic = false;
        changedPrepare = false;
        changedLongPrepare = false;
        colorising = false;
        manager.DeleteButton(this.gameObject);
        if (!manager.IsButtonInQueue(this.gameObject))
        {
            CountCounteric();
        }
        if (counteric == 0)
        {
            counterManager.ChangeTo(-1);
        }
        if (canselGreen)
        {
            lastNeedGreen = false;
            canselGreen = false;
        }
        curSoundIndex++;
    }



    public void ClearCounteric()
    {
        counteric = -1;
        counterManager.ChangeTo(counteric);
    }



    public void CountCounteric()
    {
        if (counterManager != null)
        {
            if (startTime.Count > 0)
            {
                if (startTime[0] * speeder - 1.5f * speeder <= Time.timeSinceLevelLoad || beforeProclicked)
                {
                    int i;
                    if (startTime.Count > 1)
                    {
                        float deltaMax = 0.5f;
                        int countSize = 0;
                        for (i = 0; i < startTime.Count - 1; i++)
                        {
                            if (i > 15)
                            {
                             //   Debug.Log(startTime[i + 1] + " MINUS " + startTime[i] + " IS CALCULATING");
                                if (startTime[i + 1] * speeder - startTime[i] * speeder < deltaMax * speeder)
                                {
                                  //  Debug.Log("APPLYED");
                                }
                            }
                            if (startTime[i + 1] * speeder - startTime[i] * speeder < deltaMax * speeder)
                            {
                                countSize++;
                            }
                            else
                            {
                                i = 100;
                                break;
                                break;
                            }
                        }
                        if (countSize > 0)
                        {
                            countSize++;
                        }
                        countered = true;
                        counteric = countSize;
                        if (counteric >= 1)
                        {
                            counterManager.ChangeTo(counteric - 1);
                        }
                        else
                        {
                            counterManager.ChangeTo(-1);
                        }
                    }
                    else if (startTime.Count == 1)
                    {
                        if (startTime[0] * speeder - 1.5f * speeder < Time.timeSinceLevelLoad)
                        {
                            counteric = 0;
                            counterManager.ChangeTo(counteric);
                        }
                        else
                        {
                            counteric = -1;
                            counterManager.ChangeTo(counteric);
                        }
                    }
                    else
                    {
                        counteric = -1;
                        counterManager.ChangeTo(counteric);
                    }
                }
                else
                {
                    counteric = -1;
                    counterManager.ChangeTo(counteric);
                }
            }

        }

    }




    public void CheckForTiming()
    {
       /* int i;
        if (startTime.Count != endTime.Count)
        {
        }
        else
        {

            for (i = 0; i < startTime.Count; i++)
            {
                if (endTime[i] < startTime[i])
                {
                }
                else if (endTime[i] - startTime[i] >= 3.3f)
                {
                }
            }
        }
        checkedTime = true;
        */
    }


    void FixedUpdate()
    {
        if (managerGO.transform.localScale.x <= 0)
        {
            if (this.gameObject.transform.localScale.x > 0)
            {
                this.gameObject.transform.localScale = new Vector3(-this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
            }
        }
        else
        {
            if (this.gameObject.transform.localScale.x < 0)
            {
                this.gameObject.transform.localScale = new Vector3(-this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
            }
        }
        if (playing)
        {
            if (Time.timeSinceLevelLoad > lastPlayed + playingDelay)
            {
                playing = false;
            }
        }
    }


}
