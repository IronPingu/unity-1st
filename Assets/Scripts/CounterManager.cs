﻿using UnityEngine;
using System.Collections;

public class CounterManager : MonoBehaviour {

    [SerializeField]
    private Sprite[] numbers;
    private SpriteRenderer renderer;
    private int curNum = -1;

    void Start()
    {
        renderer = this.gameObject.GetComponent<SpriteRenderer>();
    }


    public int GetCurrentNumber()
    {
        return curNum;
    }

    public void ChangeTo(int num)
    {
        if (num >=0 && num < 24)
        {
            if (renderer.sprite == null)
            {
                renderer.sprite = numbers[num];
                curNum = num;
            }
            else if (renderer.sprite != numbers[num])
            {
                renderer.sprite = numbers[num];
                curNum = num;
            }
        }
        else
        {
            if (renderer.sprite != null)
            {
                renderer.sprite = null;
                curNum = -1;
            }
        }
    }
}
