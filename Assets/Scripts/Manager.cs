﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

public class Manager : MonoBehaviour
{

    public GameObject loseMenu;
    [SerializeField]
    private TextAsset textData;
    [SerializeField]
    private List<GameObject> pressedButtons = new List<GameObject>();
    [SerializeField]
    private Text errorsText;
    private int errorsCount = 0;
    [SerializeField]
    private ButtonData b1Data;
    [SerializeField]
    private ButtonData d3Data;
    [SerializeField]
    private ButtonData f3HData;
    [SerializeField]
    private ButtonData d4Data;
    [SerializeField]
    private ButtonData a1Data;
    [SerializeField]
    private ButtonData c3HData;
    [SerializeField]
    private ButtonData e3Data;
    [SerializeField]
    private ButtonData g1Data;
    [SerializeField]
    private ButtonData b2Data;
    [SerializeField]
    private ButtonData f4HData;
    [SerializeField]
    private ButtonData e4Data;
    [SerializeField]
    private ButtonData a4Data;
    [SerializeField]
    private ButtonData e1Data;
    [SerializeField]
    private ButtonData g2Data;
    [SerializeField]
    private ButtonData a3Data;
    [SerializeField]
    private ButtonData b3Data;
    [SerializeField]
    private ButtonData d2Data;
    [SerializeField]
    private ButtonData g4Data;
    [SerializeField]
    private ButtonData wrongData;
    [SerializeField]
    private AudioClip[] music;
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private GameObject musicOn;
    [SerializeField]
    private GameObject musicOff;

    private int curTotalCount = 0;
    [SerializeField]
    private List<GameObject> queuer = new List<GameObject>();
    [SerializeField]
    private List<GameObject> queuerOne = new List<GameObject>();

    private float addedTime = 0f;
    [SerializeField]
    private Text debugText;

    [SerializeField]
    private Text timer;

    [SerializeField]
    private GameObject buttonsGO;
    [SerializeField]
    private GameObject menuGO;
    private float lastTimeAdded = 0f;
    [SerializeField]
    private float timeToColor = 0.3f;
    [SerializeField]
    private float longTimeToColor = 1.5f;

    public void AgainPressed()
    {
        Application.LoadLevel("Game");
    }

    public void MainMenuPressed()
    {
        Application.LoadLevel("Main");
    }


    public float GetLongTimeToColor()
    {
        return longTimeToColor;
    }
    public float GetTimeToColor()
    {
        return timeToColor;
    }

    public void MenuPressed()
    {
        menuGO.SetActive(false);
        buttonsGO.SetActive(true);
    }

    public void AddError()
    {
        if (Time.timeScale != 0)
        {
            errorsCount++;
            errorsText.text = errorsCount.ToString();   
        }
        if (errorsCount >= 110)
        {
            Time.timeScale = 0f;
            source.volume = 0f;
            loseMenu.SetActive(true);
        }
    }

    public void ClosePressed()
    {
        menuGO.SetActive(true);
        buttonsGO.SetActive(false);
    }

    public bool IsButtonInQueue(GameObject button)
    {
        bool finded = false;
        int i;
        for (i = 0; i < queuer.Count; i++)
        {
            if (queuer[i] == button)
            {
                finded = true;
            }
        }
        return finded;
    }

    public void AddButtonQueue(GameObject button)
    {
      /*  if (lastTimeAdded + 0.01f <= Time.time)
        {
            lastTimeAdded = Time.time;
            queuer.Add(button);
            if (queuer.Count > 1)
            {
                int i;
                for (i = 0; i < queuer.Count; i++)
                {
                    queuer[i].GetComponent<ButtonData>().UpdateQueuer(i);
                }
                
            }
        }
        else if (lastTimeAdded + 0.01f >= Time.time)
        {
            if (queuer[queuer.Count - 1] != button)
            {*/

                /*if (queuer[queuer.Count - 1].gameObject.GetComponent<ButtonData>().GetCurrentQueuer() == 0)
                {
                    queuer[queuer.Count - 1].gameObject.GetComponent<ButtonData>().UpdateQueuer(-1);
                    queuer.RemoveAt(queuer.Count - 1);
                }*/
                /*queuerOne.Add(button);
                int number = queuer.Count - 1;
                int i;
                button.GetComponent<ButtonData>().UpdateQueuer(number);
                for (i = 0; i < queuerOne.Count; i++)
                {
                    queuerOne[i].GetComponent<ButtonData>().UpdateQueuer(number);
                }
            }
        }*/
        if (!IsButtonInQueue(button))
        {
            lastTimeAdded = Time.time;
            queuer.Add(button);
            if (queuer.Count > 1)
            {
                int i;
                for (i = 0; i < queuer.Count; i++)
                {
                    queuer[i].GetComponent<ButtonData>().UpdateQueuer(i);
                    //Debug.Log(queuer[i].name + " IS UPDATED TO" + (i + 1));
                }
              //  button.GetComponent<ButtonData>().UpdateQueuer(i);

            }
        }
    }

    public void DeletePreviousButton(GameObject queued)
    {
        if (queuer.Count > 0 && queuer.Contains(queued))
        {
            queuer[0].GetComponent<ButtonData>().UpdateQueuer(-1);
            queuer.RemoveAt(0);
            int i;
            for (i = 0; i < queuer.Count; i++)
            {
                queuer[i].GetComponent<ButtonData>().UpdateQueuer(i);
            }
           /*int number = queuer.Count - 1;
            for (i = 0; i < queuerOne.Count; i++)
            {
                queuerOne[i].GetComponent<ButtonData>().UpdateQueuer(-1);
            }*/
        }
    }

    public void DeleteButton(GameObject button)
    {
        if (queuer.Contains(button))
        {

            button.GetComponent<ButtonData>().UpdateQueuer(-1);
           // if (queuer.IndexOf(button) == 0)
        //    {

                queuer.Remove(button);
                queuerOne.Remove(button);
                int i;
                for (i = 0; i < queuer.Count; i++)
                {
                    queuer[i].GetComponent<ButtonData>().UpdateQueuer(i);
                }
                /*int number = queuer.Count - 1;
                for (i = 0; i < queuerOne.Count; i++)
                {
                    queuerOne[i].GetComponent<ButtonData>().UpdateQueuer(-1);
                }*/
        //    }
        }
       
    }

    void Start()
    {
        QualitySettings.vSyncCount = 0;  
        Application.targetFrameRate = 45;
        source.Stop();
        if (source == null)
        {
            source = this.gameObject.GetComponent<AudioSource>();
        }
        source.volume = 1f;
        /*if(PlayerPrefs.GetInt("music",0)==1)
        {
            source.volume = 1f;
           // Debug.Log("MUSIC TURNED ON");
            musicOn.SetActive(false);
            musicOff.SetActive(true);
        }
        else
        {
            source.volume = 0f;
            //Debug.Log("MUSIC TURNED OFF");
            musicOn.SetActive(true);
            musicOff.SetActive(false);
        }*/
        RefreshTime();
        Time.timeScale = 1f;
        Application.targetFrameRate = 60;
        float w = Screen.width;
        float h = Screen.height;
        float mult = w / h;
        this.gameObject.transform.localScale = new Vector3(mult , 1f, 1f);
        ReadStyleSheet();

        if (PlayerPrefs.GetInt("leftHand") == 1)
        {
           // Debug.Log("LEFT HANDED TRUEEE");
            this.gameObject.transform.localScale = new Vector3(-this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);

        }
        //Time.timeScale = 0.5f;
        int _timeMode = PlayerPrefs.GetInt("speedMode", 2);
        switch (_timeMode)
        {
            case 0:
               // Time.timeScale = 0.5f;
            source.PlayOneShot(music[0]);
            source.Play();
                break;
            case 1:
               // Time.timeScale = 0.75f;
            source.PlayOneShot(music[1]);
            source.Play();
                break;
            case 2:
               // Time.timeScale = 1f;
            source.PlayOneShot(music[2]);
            source.Play();
                break;
        }
        PlayerPrefs.SetInt("lastTimeMode", _timeMode);
        PlayerPrefs.Save();
    }

    void ReadStyleSheet()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textData.text);
        int i;
        XmlNodeList notes = xmlDoc.GetElementsByTagName("note");
        for (i = 0; i < notes.Count; i++)
        {
            XmlNodeList noteData = notes[i].ChildNodes;
            ButtonData currentButton;
            switch(noteData[0].InnerText){
                case "b1":
                    currentButton = b1Data;

                    break;
                case "d3":
                    currentButton = d3Data;

                    break;
                case "f3H":
                    currentButton = f3HData;

                    break;
                case "d4":
                    currentButton = d4Data;

                    break;
                case "a1":
                    currentButton = a1Data;

                    break;
                case "c3H":
                    currentButton = c3HData;

                    break;
                case "e3":
                    currentButton = e3Data;

                    break;
                case "g1":
                    currentButton = g1Data;

                    break;
                case "b2":
                    currentButton = b2Data;

                    break;
                case "f4H":
                    currentButton = f4HData;

                    break;
                case "e4":
                    currentButton = e4Data;

                    break;
                case "a4":
                    currentButton = a4Data;

                    break;
                case "e1":
                    currentButton = e1Data;

                    break;
                case "g2":
                    currentButton = g2Data;

                    break;
                case "a3":
                    currentButton = a3Data;

                    break;
                case "b3":
                    currentButton = b3Data;

                    break;
                case "d2":
                    currentButton = d2Data;

                    break;
                case "g4":
                    currentButton = g4Data;

                    break;
                default:
                        currentButton = wrongData;
                        break;

            }
            float prevValue = 0f;
            string[] timers = noteData[1].InnerText.Split(' ');
            int j;
            for (j = 0; j < timers.Length; j++)
            {
                float valuer = 0f;
                if (j % 2 == 0)
                {
                    if (float.TryParse(timers[j], out valuer))
                    {
                        if (valuer < prevValue)
                        {
                            //Debug.Log(currentButton.gameObject.name + " is wrong with timer" + valuer);
                        }
                        currentButton.AddStartTime(valuer);
                        prevValue = valuer;
                    }
                    else
                    {
                       // Debug.Log(timers[j] + " is wrong counted" + currentButton.gameObject.name);
                    }
                }
                else
                {
                    if (float.TryParse(timers[j], out valuer))
                    {
                        if (valuer < prevValue)
                        {
                            //Debug.Log(currentButton.gameObject.name + " is wrong with timer"+valuer);
                        }
                        currentButton.AddEndTime(valuer);
                        prevValue = valuer;
                    }
                    else
                    {
                       // Debug.Log(timers[j] + " is wrong counted" + currentButton.gameObject.name);
                    }
                }
            }
        }
    }

    public void ChangeHand()
    {
        this.gameObject.transform.localScale = new Vector3(-this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
        if (this.gameObject.transform.localScale.x < 0)
        {
            PlayerPrefs.SetInt("leftHand", 1);
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.SetInt("leftHand", 0);
            PlayerPrefs.Save();
        }
    }

    public void Reload()
    {
        Application.LoadLevel(Application.loadedLevel);
        //RefreshTime();
    }

    public void MusicOn()
    {
        PlayerPrefs.SetInt("music", 1);
        PlayerPrefs.Save();
        Reload();
    }

    public void MusicOff()
    {
        PlayerPrefs.SetInt("music", 0);
        PlayerPrefs.Save();
        Reload();
    }
    void RefreshTime()
    {
        addedTime = Time.time;
        Debug.Log(addedTime + " IS ADDED TIME BASIC");
        int _timeMode = PlayerPrefs.GetInt("lastTimeMode", 2);
        switch (_timeMode)
        {
            case 0:
                //addedTime = addedTime/0.5f;
                break;
            case 1:
                //addedTime = addedTime/0.75f;
                break;
            case 2: 
                break;
        }  
        b1Data.UpdateTimer(Time.time);
        d3Data.UpdateTimer(Time.time);
        f3HData.UpdateTimer(Time.time);
        d4Data.UpdateTimer(Time.time);
        d3Data.UpdateTimer(Time.time);
        a1Data.UpdateTimer(Time.time);
        b2Data.UpdateTimer(Time.time);
        c3HData.UpdateTimer(Time.time);
        e3Data.UpdateTimer(Time.time);
        g1Data.UpdateTimer(Time.time);
        f4HData.UpdateTimer(Time.time);
        e4Data.UpdateTimer(Time.time);
        a4Data.UpdateTimer(Time.time);
        e1Data.UpdateTimer(Time.time);
        g2Data.UpdateTimer(Time.time);
        a3Data.UpdateTimer(Time.time);
        b3Data.UpdateTimer(Time.time);
        d2Data.UpdateTimer(Time.time);
        g4Data.UpdateTimer(Time.time);
        if (PlayerPrefs.GetInt("music", 0) == 0)
        {
            b1Data.MakeSoundAble(true);
            d3Data.MakeSoundAble(true);
            f3HData.MakeSoundAble(true);
            d4Data.MakeSoundAble(true);
            a1Data.MakeSoundAble(true);
            b2Data.MakeSoundAble(true);
            c3HData.MakeSoundAble(true);
            e3Data.MakeSoundAble(true);
            g1Data.MakeSoundAble(true);
            b2Data.MakeSoundAble(true);
            f4HData.MakeSoundAble(true);
            e4Data.MakeSoundAble(true);
            a4Data.MakeSoundAble(true);
            e1Data.MakeSoundAble(true);
            a3Data.MakeSoundAble(true);
            g2Data.MakeSoundAble(true);
            b3Data.MakeSoundAble(true);
            d2Data.MakeSoundAble(true);
            g4Data.MakeSoundAble(true);
        }
        else
        {

            b1Data.MakeSoundAble(false);
            d3Data.MakeSoundAble(false);
            f3HData.MakeSoundAble(false);
            d4Data.MakeSoundAble(false);
            d3Data.MakeSoundAble(false);
            a1Data.MakeSoundAble(false);
            b2Data.MakeSoundAble(false);
            c3HData.MakeSoundAble(false);
            e3Data.MakeSoundAble(false);
            g1Data.MakeSoundAble(false);
            b2Data.MakeSoundAble(false);
            f4HData.MakeSoundAble(false);
            e4Data.MakeSoundAble(false);
            a4Data.MakeSoundAble(false);
            e1Data.MakeSoundAble(false);
            a3Data.MakeSoundAble(false);
            g2Data.MakeSoundAble(false);
            b3Data.MakeSoundAble(false);
            d2Data.MakeSoundAble(false);
            g4Data.MakeSoundAble(false);
        }
    }


    public void RemovePressed(GameObject go)
    {
        pressedButtons.Remove(go);
    }

    void FixedUpdate()
    {
        timer.text = (Time.time- addedTime).ToString("0.00");
    }

    public void DebugTextChange(string text)
    {
        debugText.text = text;
    }


    public void ChangeToLost()
    {

    }

    public void OpenMenu()
    {
        Application.LoadLevel("Main");
    }

#if UNITY_EDITOR
	// Update is called once per frame
	void Update () {
        /*if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            if (Time.timeScale == 1)
                Time.timeScale = 10f;
            else
                Time.timeScale = 1f;
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (Time.timeScale != 0.1f)
                Time.timeScale = 0.1f;
            else
                Time.timeScale = 1f;
        }*/
        if (Input.GetKeyDown(KeyCode.Mouse0) )
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.tag == "Button")
                {
                    ButtonData button = hit.collider.gameObject.GetComponent<ButtonData>();
                    button.MakeSound();
                    pressedButtons.Add(hit.collider.gameObject);
                }
            }
        } 
        /*if (pressedButtons.Count > 0)
        {
            int j;
            List<GameObject> offing = new List<GameObject>();

            for (j = 0; j < pressedButtons.Count; j++)
            {
                offing.Add(pressedButtons[j]);
            }

            for (j = 0; j < 1; j++)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (hit.collider != null)
                {
                    offing.RemoveAt(j);
                }
            }
            for (j = 0; j < offing.Count; j++)
            {
                offing[j].GetComponent<ButtonData>().StopSound();
                //Debug.Log("STOP SOUND");
                pressedButtons.Remove(offing[j]);
            }
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                hit.collider.gameObject.GetComponent<ButtonData>().StopSound();
                pressedButtons.Remove(hit.collider.gameObject);
            } 
        }
        */
       
        
	}
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
	void Update() {
        Touch[] touches = Input.touches;
        int i;
        if(touches!=null)
        {
            for (i = 0; i < touches.Length; i++)
            {
                Touch touch = touches[i];
                if (touch.phase == TouchPhase.Began)
                {
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);
                    if (hit.collider != null)
                    {
                        if (hit.collider.gameObject.tag == "Button")
                        {
                            ButtonData button = hit.collider.gameObject.GetComponent<ButtonData>();
                            button.MakeSound();
                            //pressedButtons.Add(hit.collider.gameObject);
                        }
                    }
                }
            }
        }

         /*if (pressedButtons.Count > 0)
        {
            int j;
            List<GameObject> offing = new List<GameObject>();

            for (j = 0; j < pressedButtons.Count; j++)
            {
                offing.Add(pressedButtons[j]);
            }

            for (j = 0; j < touches.Length; j++)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touches[j].position), Vector2.zero);
                if (hit.collider != null)
                {
                    offing.RemoveAt(j);
                }
            }
            for (j = 0; j < offing.Count; j++)
            {
                offing[j].GetComponent<ButtonData>().StopSound();
                pressedButtons.Remove(offing[j]);
            }
        } 
        for(i=0;i<touches.Length;i++){
            if(touches[i].phase == TouchPhase.Ended || touches[i].phase == TouchPhase.Canceled){
                
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touches[i].position), Vector2.zero);
                if (hit.collider != null)
                {
                    hit.collider.gameObject.GetComponent<ButtonData>().StopSound();
                    pressedButtons.Remove(hit.collider.gameObject);
                } 
            }
        }*/
	}
#endif

#if UNITY_IOS && !UNITY_EDITOR
	void Update () {
        Touch[] touches = Input.touches;
        int i;
	    if (touches != null)
	    {
            for (i = 0; i < touches.Length; i++)
            {
                Touch touch = touches[i];
                if (touch.phase == TouchPhase.Began)
                {
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);
                    if (hit.collider != null)
                    {
                        if (hit.collider.gameObject.tag == "Button")
                        {
                            ButtonData button = hit.collider.gameObject.GetComponent<ButtonData>();
                            button.MakeSound();
                            pressedButtons.Add(hit.collider.gameObject);
                        }
                    }
                }
            }   
	    }

         /*if (pressedButtons.Count > 0)
        {
            int j;
            List<GameObject> offing = new List<GameObject>();

            for (j = 0; j < pressedButtons.Count; j++)
            {
                offing.Add(pressedButtons[j]);
            }

            for (j = 0; j < touches.Length; j++)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touches[j].position), Vector2.zero);
                if (hit.collider != null)
                {
                    offing.RemoveAt(j);
                }
            }
            for (j = 0; j < offing.Count; j++)
            {
                offing[j].GetComponent<ButtonData>().StopSound();
                pressedButtons.Remove(offing[j]);
            }
        } 
        for(i=0;i<touches.Length;i++)
        {
            if(touches[i].phase == TouchPhase.Ended || touches[i].phase == TouchPhase.Canceled){
                
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touches[i].position), Vector2.zero);
                if (hit.collider != null)
                {
                    hit.collider.gameObject.GetComponent<ButtonData>().StopSound();
                    pressedButtons.Remove(hit.collider.gameObject);
                } 
            }
        }*/
	}
#endif
}
